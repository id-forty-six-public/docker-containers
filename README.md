# docker-containers

docker builds for testing / deploy

## ansible 2.9

```
cd ansible-ubuntu
sudo docker build --cache-from=ademas/ansible-2.9 -t ademas/ansible-2.9 .
sudo docker push ademas/ansible-2.9
```

## mongoexpress

```
cd mongoexpress
sudo docker build --cache-from=ademas/mongo-express -t ademas/mongo-express .
sudo docker push ademas/mongo-express
```
